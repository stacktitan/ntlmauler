from distutils.core import setup

setup(
    name="NTLMauler",
    version="0.1.2",
    author="Dan Kottmann",
    author_email="djkottmann@gmail.com",
    packages=["ntlmauler"],
    scripts=["bin/ntlmauler"],
    license="LICENSE.txt",
    url="http://bitbucket.org/dkottmann/ntlmauler",
    description="NTLM Web Authentication dictionary-based password guessing",
    install_requires=[
        "docopt >= 0.6.1",
        "gevent >= 0.13.8",
        "greenlet >= 0.4.1",
        "grequests >= 0.2.0",
        "python-ntlm >= 1.0.1",
        "requests >= 1.2.3",
        "requests-ntlm >= 0.0.2.2"
    ]
)