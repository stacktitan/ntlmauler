#NTLMauler#

Performs dictionary attacks against NTLM-based web authentication. You provide to it a list of users and passwords and it attempts
to authenticate to the target server using every combination. The application is asynchronous and can execute up to 20 authentication
requests in parallel. Additionally, options allow a user to limit the number of attempts for users in order to reduce the likelihood
of account lockouts.

Sample run:

        bash-3.2# python ./ntlmauler http://10.0.1.20 --users users.txt --wordlist dict.txt --concurrency 20 --verbose
        [+] Determining baseline response code...
        [+] Baseline response code is 401
        [+] Running with maximum 20 conncurrent requests
        [+] Beginning authentication attempts for 9 unique user/pass combos...
        => Attempting tadams : Password123
        => Attempting test : Password123
        => Attempting test2 : Password123
        => Attempting tadams : Password1
        => Attempting test : Password1
        => Attempting test2 : Password1
        [+] SUCCESSFULLY AUTHENTICATED WITH WORKGROUP\tadams : Password1
        => Attempting test : alphabetsoup
        => Attempting test2 : alphabetsoup

        [+] The following 1 login(s) succeeded:
            WORKGROUP\tadams:Password1

###Installation###
1. Install pip
2. Install NTLMauler with pip using the dist tarball

        sudo pip install ntlmauler_<version>.tar.gz

###Running###

1. Execute nsen via the command line

        ntlmauler [options]

2. View usage details

        ntlmauler --help

3. Execute a dictionary attack with 10 concurrent requests using the domain name "CORP":

        ntlmauler --url http://example.com --users users.txt --wordlist passwords.lst --domain CORP --concurrency 10

4. Execute an attack but limit attempts to 2 per user to try to avoid lockout:

        ntlmauler --url http://example.com --users users.txt --wordlist passwords.lst --domain CORP --max-attempts 2

5. Execute an exhaustive attack but stop after the first successful authentication:

        ntlmauler --url http://example.com --users users.txt --wordlist passwords.lst --domain CORP --victory

6. *DANGEROUS* Run with 20 concurrent requests and don't adjust concurrency to protect from lockouts:

        ntlmauler --url http://example.com --users users.txt --wordlist passwords.lst --domain CORP --concurrency 20 --yolo


###Miscellaneous###

By default NTLMauler will try to prevent account lockouts by adjusting the number of concurrent requests accordingly. Consider the
following example.

Assume GPO is configured to lock accounts out after 3 failed attempts and an account with credentials bob:Password1 exists. Now,
assume we run the command with the default concurrency value (5 is default):

        ntlmauler --url example.com --users u.txt --wordlist list.txt

Assume only one user (bob) is being tested. This will lock the account out with certainty even if the correct password is supplied on
a request in the first batch of concurrent requests. As an example, assume the following 5 requests are all sent
simultaneously:

        REQ1 => bob:Password1
        REQ2 => bob:Password123
        REQ3 => bob:letmein
        REQ4 => bob:s3cr3t
        REQ5 => bob:opensesame

Even though the first request has the correct credentials they are all in-transit simultaneously. Regardless of the order the
requests are received by the server there is guaranteed to be 3 invalid login attempts in a row which will lock out the account.
To prevent this, NTLMauler will never send multiple authentication requests simultaneously to the server for the same user. When
testing against only a single user account this basically eliminates all concurrency.
However, concurrent requests will be made in the event multiple users are present. For example, this can happen in parallel:

        REQ1 => bob:Password1
        REQ2 => alice:Password1
        REQ3 => joe:Password1
        REQ4 => tom:Password1
        REQ5 => amanda:Password1

These protections imply restrictions on the number of concurrent HTTP requests. Indeed, that is correct. The value supplied in
the --concurrency option is considered as a maximum value tolerable by the user. In reality, the number of concurrent
requests will be adjusted to one of the following (whichever is lowest):

1. The value supplied by the user in the --concurrency option (or 5 if not supplied)
2. The number of unique user names identified in the users file (from the --users option)
3. The number of remaining, unique user names that have not been successfully authenticated (calculated dynamically)

It should be noted that the problem described above is not specific to scenarios where only a single user is being
tested. This can occur with multiple users as well (e.g. 2 users and 10 concurrent requests has the same problem and would
lock both accounts out if not protected).

**USE THE FOLLOWING AT YOUR OWN RISK - YOU HAVE BEEN WARNED**

The --yolo option completely disables the lockout protections described above. As a result, NTLMauler will run with
the user-defined number of concurrent requests (or 5 if not provided in --concurrency option). There is a legitimate use
case for --yolo. If account lockouts are disabled then this option would maximize speed by preventing NTLMauler from
unnecessarily reducing concurrency when there is no risk.

