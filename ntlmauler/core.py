#!/usr/bin/env python
# Copyright (c) 2013 Dan Kottmann
# See the file license.txt for copying permission

import grequests as requests
from ntlmauler.util import success, warning
from requests_ntlm import HttpNtlmAuth


def auth_async(targets):
    """
    Perform asynchronous authentication against a list of targets
    :type targets: list(NtlmTargets)
    :param targets: List of NtlmTargets to test for authentication
    """
    reqs = list()

    # Build a list of grequest items
    for target in targets:
        reqs.append(requests.get(target.url, auth=HttpNtlmAuth(target.user_domain, target.password), verify=False))

    # Run auth attempts asynchronously
    results = requests.map(reqs)

    # Set status code on targets from results
    for idx, result in enumerate(results):
        targets[idx].status_code = result.status_code

    return targets

