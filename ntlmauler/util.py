#!/usr/bin/env python
# Copyright (c) 2013 Dan Kottmann
# See the file license.txt for copying permission


def croak(data):
    """
    Display an error message and abort
    """
    print error("[!] Error: {0}".format(data))
    exit(1)


def warning(data):
    """
    format a string for 'yellow' colored terminal output
    """
    return '\033[93m' + data + '\033[0m'


def error(data):
    """
    format a string for 'red' colored terminal output
    """
    return '\033[91m' + data + '\033[0m'


def success(data):
    """
    format a string for 'green' colored terminal output
    """
    return '\033[92m' + data + '\033[0m'


def process_results(targets, valid_creds, baseline=401):
    """
    processes a target list, pretty prints results, and builds list of success
    :param targets: List of NtlmTargets post authentication-attempt
    :param valid_creds: Dict of successful authentication attempts
    :param baseline: The baseline status code for failed auth
    :rtype : Boolean value indicating if any attempts were successful
    """
    is_success = False
    for target in targets:
        # Check if response status code was 200 (success)
        if target.status_code == 200:
            print success("[+] SUCCESSFULLY "
                          "AUTHENTICATED WITH {0} : "
                          "{1}".format(target.user_domain, target.password))

            is_success = True
            # Add creds to dict
            valid_creds[target.user_domain] = target.password

        elif target.status_code != baseline:
            # If there was a variation from the baseline and it was not
            # successful then print a warning
            print warning("[!] VARIATION FROM "
                          "BASELINE DETECTED WITH {0} : "
                          "{1}".format(target.user_domain, target.password))

    return is_success
