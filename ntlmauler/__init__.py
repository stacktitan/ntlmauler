#!/usr/bin/env python
# Copyright (c) 2013 Dan Kottmann
# See the file license.txt for copying permission


class NtlmTarget():

    def __init__(self, user, password, url, domain="WORKGROUP", baseline=401):
        self.user = user
        self.password = password
        self.domain = domain
        self.url = url
        self.status_code = baseline
        self.user_domain = domain + "\\" + user
